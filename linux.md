[[_TOC_]]

# Installation / désinstallation

| Commande | Explication |
|--------------|-----------|
| sudo dpkg -i nom_fic.deb | installe un .deb |
| sudo alien -d nom_fic.tar.gz | converti les .rpm, .slp, .pkg et .tgz en .deb |
| sudo alien -i nom_fic.tar.gz | installe directement les .rpm, .slp, .pkg et .tgz |
| sudo apt-get install X | installe X |
| sudo apt-get --purge remove X | désinstalle X |
| sudo apt-get install -f | installe/répare les dépendances |
| apm install nom_pckg | installe un pckg atom |

Si après `sudo apt install`, il y des mises à jour à faire (warnings), lancer : `sudo apt upgrade`. Si cette dernière commande renvoie une liste de paquets non mis à jour il faut les faire un par un, soit : `sudo apt list --upgradable` pour lister les paquets et `sudo apt upgrade <nomPaquet>` pour les mettre à jour.
> Si cela produit une erreur de type paquet défectueux, lancer la commande : `sudo apt --only-upgrade install <nomPaquet>`

# Gestion de documents

| Commande | Explication |
|--------------|-----------|
| rmdir doss | Supprime le dossier doss uniquement s'il est vide |
| rm -r doss | Supprime le dossier doss et tous ses fichiers |
| rmdir doss | Supprime le dossier doss uniquement s'il est vide |
| rm fic | Supprime le fichier fic |
| cp -r doss1 doss2 | Copie le doss1 dans le doss2 |
| cp doss1/* doss2 | Copie tous les fichiers du doss1 dans le doss2 |
| ln -s chem1 chem2 | Créer un lien symbolique à l'emplacement chem2 à partir du chemin cible chem1 |
| sudo mount -t cifs 'adresseReseau' dossier -o user=user,vers=x.x | Monter le disque réseau nomReseau dans dossier |

Si le montage réseau ne fonctionne pas il faut installer l'utilitaire pour les partitions cifs : `apt install mount.cifs` puis `mount -a`. Si ça ne fonctionne toujours pas, il faut essayer de changer la version utilisée en ajoutant l'option `vers=1.0` ou `vers=3.0`.

# Gestion des droits

| Commande | Explication |
|--------------|-----------|
| sudo chgrp nouvGrp fic | change le groupe actuel du fichier fic pour nouvGrp |
| sudo chown pers fic | change le propriétaire actuel du fichier fic pour pers |
| sudo chmod droits fic | change les droits du fichier fic pour droits (cf. ci-dessous) |

On choisit :
* À qui s'applique le changement
    * u (user, utilisateur) représente la catégorie "propriétaire" ;
    * g (group, groupe) représente la catégorie "groupe propriétaire" ;
    * o (others, autres) représente la catégorie "reste du monde" ;
    * a (all, tous) représente l'ensemble des trois catégories.
* La modification que l'on veut faire
    * \+ : ajouter
    * \- : supprimer
    * = : affectation
* Le droit que l'on veut modifier
    * r : read ⇒ lecture
    * w : write ⇒ écriture
    * x : execute ⇒ exécution
    * X : eXecute ⇒ exécution, concerne uniquement les répertoires (qu'ils aient déjà une autorisation d'exécution ou pas) et les fichiers qui ont déjà une autorisation d'exécution pour l'une des catégories d'utilisateurs. Nous allons voir plus bas dans la partie des traitements récursifs l'intérêt du X.
=> `chmod u+rwx,g+rx-w,o+r-wx fic` donne tous les droits : lecture, écriture et exécution pour le propriétaire, lecture et exécution au groupe propriétaire + retrait écriture, lecture aux autres + retrait écriture et exécution.

En octal, chaque « groupement » de droits (pour user, group et other) sera représenté par un chiffre et à chaque droit correspond une valeur :
* r (read) = 4
* w (write) = 2
* x (execute) = 1
* \- = 0
Par exemple,
* Pour rwx, on aura : 4+2+1 = 7
* Pour rw-, on aura : 4+2+0 = 6
* Pour r--, on aura : 4+0+0 = 4
=> `chmod 777 fic` donne tous les droits à tout le monde sur fic.

# Processus

| Commande | Explication |
|--------------|-----------|
| ps -ef | liste tous les processus en cours d'exécution |
| kill -9 numPid | stoppe le processus avec le pid numPid |
| pkill nomProc | stoppe le processus nomProc |
| xkill | stoppe le processus de la fenêtre sur laquelle on clique |

# Installer Linux (via clef USB bootable)

Installer un logiciel qui rend bootable la clef USB avec une grande variété de systèmes disponibles : `sudo apt install rpi-imager`.

## Dual boot

## Remplacer Windows par Linux

# Localhost

* Si localhost tombe faire un `sudo systemctl status apache2` pour vérifier l'état d'apache.
    * Si erreur dans un fichier .conf sur la ligne `ProxPreservePass`, c'est que `proxy_http` est tombé => lancer `a2enmod proxy_http` puis `systemctl restart apache2`.

# Autres

| Commande | Explication |
|--------------|-----------|
| sudo !! | relance la commande précédente |
| ctrl-z | ferme un processus (ne le stoppe pas forcément) |
| ctrl-c | stoppe un processus, sort de force d'une commande et ne l'exécute pas |
| ctrl-d | ferme une commande (qui s'écrit sur plusieurs lignes par exemple) et l'exécute |
| ctrl-↑-w | ferme le terminal courant |
| sudo -i | fait passer le terminal en mode root |
| php -S localhost:8000 -d | lance un serveur interne sur le port 8000 en mode démon (s’exécute en background) |
| ssh-add | plus besoin d'entrer le mot de passe ssh durant un certain temps |
