[[_TOC_]]

# Quelques notions en vrac

Passer par docker permet d'éviter les contraintes comme par exemple l'utilisation conjointe de PHP 5 et 7 (compliqués à faire cohabiter sur une même machine).

A l'entrée d'un docker on indique le nom d'un service ou le nom du docker.

Il faut mettre le docker à l'endroit où se situe le docker-compose.yml.

Dans le docker chaque ligne est un service.

Docker : gestion de conteneurs individuels.


Un service prend du code pour le mettre en dehors du contrôleur afin de le laisser plus propre.
*use* permet d'appeler le service.

# Composer.yml

C'est un fichier qui gère un ensemble de dockers. Il contient donc un ensemble de services permettant à php d'installer les dépendances nécessaires.
! Ne pas mettre de majuscule dans les noms de site et de domaine.

* *image :* lien vers une image source existante pour le docker
* *restart :* permet de relancer le fichier lorsque l'on relance la machine hôte
* *volumes : chemin1:chemin2* mappe les chemin2 du conteneur dans le chemin1 de la machine hôte. Peut aussi servir à injecter un fichier de la machine hôte vers le conteneur. Commande forcément bidirectionnelle.
* *links : db:db* appelle un service depuis un autre avec tel nom
* *build :* construit le docker
* *up:* lance les service du docker
* *apache :*, *db :* sont deux conteneurs dissociés.
* *localhost: n°port* définit l'environnement : ouvre le apache du docker

Aide [Openclassrooms](https://openclassrooms.com/fr/courses/2035766-optimisez-votre-deploiement-en-creant-des-conteneurs-avec-docker/6211677-creez-un-fichier-docker-compose-pour-orchestrer-vos-conteneurs)

# Composer.json vs composer.lock

Stocke les dépendances à une version donnée (ex. : "^3.2") - Stocke précisément quelle version du logiciel est installée (ex. : "v.3.2.1").
! Ne pas mettre de majuscule dans les noms de site et de domaine.

# Composer.yml

C'est un fichier qui gère un ensemble de dockers. Il contient donc un ensemble de services permettant à php d'installer les dépendances nécessaires.
! Ne pas mettre de majuscule dans les noms de site et de domaine !

# Commandes

! Penser à exécuter les commandes docker avec sudo !

| Commande | Explication |
|--------------|-----------|
| docker-compose build | construit le docker |
| docker-compose up -d | lance les services de docker |
| docker-compose exec apache bach | entre dans le docker apache et on ouvre sa console bash |
| htpasswd -c -b -B /etc/apache2/htpass/.htpasswd user passwd | crée le fichier htpasswd (*-c* : ou le remplace), *-b* permet d'écrire le mot de passe dans la ligne de commande (! il est visible !) au lieu de le demander après. *-B* chiffre le mot de passe. |
| composer require | installe la version précisée en ligne de commande ou la dernière version existante et met à jour le composer.lock |
| composer install | installe la version précisée dans le composer.lock |
| composer update | télécharge la dernière version existante et met à jour le composer.lock |
