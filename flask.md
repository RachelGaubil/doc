[[_TOC_]]

[basé sur https://flask-fr.readthedocs.io/]

# Démarrer

## Installation

Privilégier au minimum Python 3.6.

Se mettre dans le dossier du projet pour créer un environnement virtuel puis l'activer :

 ```
 $ python3 -m venv nomEnvironnement
 $ . nomEnvironnement/bin/activate
 ```

Installer Flask : `pip install Flask`.

## Lancement

Contenu minimal d'une application Flask :
```
from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"
```

Pour lancer l'application :
```
$ export FLASK_APP=nomApp
$ flask run
```
`FLASK_APP` sert à indiquer à Flask quelle est l'application à lancer. Dans le cas d'un module python, `nomApp` correspond au nom du script et dans le cas d'un paquet, il correspond au nom du dossier contenant le paquet.

*Remarque : si le script est nommé `app.py` ou `wsgi.py` l'utilisation de `FLASK_APP` n'est pas nécessaire.*

**! Ne pas nommer l'application `flask.py` pour éviter le conflit avec Flask !**

Pour lancer Flask en mode débogage on peut utiliser `export FLASK_DEBUG=True`. Ce mode permet à l'application de se mettre à jour d'elle-même à chaque modification du code python.

# Notions à connaître

## Routes

Les décorateurs route indiquent à Flask quelle URL doit déclencher la fonction qui la suit.

On peut y indiquer des URLs uniques ou bien avec redirection :
* `@app.route('/login/')` fait en sorte que si l'utilisateur tente d'accéder à `/login` il sera redirigé vers `/login/`.
* `@app.route('/logout')` est une URL unique et si l'utilisateur tente d'accéder à `/logout/` cela produira une erreur 404.

## Échappement

Lorsque des variables sont utilisées dans les routes, l'échappement est nécessaire pour éviter les attaques par injections. Pour cela il faut soit utiliser les modèles HTML soit **escape()**.
```
from markupsafe import escape

@app.route("/<name>")
def hello(name):
    return f"Hello, {escape(name)}!"
```
L'échappement permet à la variable entrée par l'utilisateur d'être considérée comme du texte et non pas un script à exécuter.

## Variables

Les variables récupérées dans les URL peuvent être converties (elles sont pas défaut considérées comme des chaînes de caractères). Exemple d'une conversion en entier :
```
@app.route('/post/<int:post_id>')
def show_post(post_id):
    return f'Post {post_id}'
```
Liste des convertisseurs :

| Convertisseur | Ce qu'il accepte |
|--------------|-----------|
| `string` | tout texte sans slash |
| `int` | les nombres entiers positifs |
| `float` | les valeurs positives à virgule flottante |
| `path` | tout texte (y compris avec slashs) |
| `uuid` | les chaînes UUID |

## Création d'URL

La fonction **url_for()** permet de créer une URL. Elle prend comme premier argument le nom de la fonction, puis des variables connues ou non.
```
from flask import url_for

@app.route('/login')
def login():
    return 'login'

@app.route('/user/<username>')
def profile(username):
    return f'{username}\'s profile'

with app.test_request_context():
    print(url_for('login'))
    print(url_for('login', next='/'))
    print(url_for('profile', username='John Doe'))
```
**test_request_context()** indique à Flask permet d'afficher le résultat dans la console.
```
/login
/login?next=/
/user/John%20Doe
```

## Méthodes HTTP

Par défaut, les routes ne répondent qu'aux requêtes `GET` mais cela est paramétrable grâce à l'argument `methods`.
```
from flask import request

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        return do_the_login()
    else:
        return show_the_login_form()
```

Générer une URL pour un fichier statique se fait de la façon suivante : `url_for('static', filename='style.css')`

## Fichiers statiques

Les fichiers statiques sont en général les CSS et JavaScript.
Ils doivent être dans le dossier `/static`.

## Modèles HTML

Les modèles permettent d'éviter de générer le HTML dans le script car c'est encombrant. Ils s'utilisent avec la méthodes **render_template()** :
```
from flask import render_template

@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    return render_template('hello.html', name=name)
```
Les modèles doivent être dans le dossier `templates`. La hiérarchie des documents n'est pas la même en fonction de si l'application est un module ou un paquet.
* Dans le cas d'un module :
```
/app.py
/templates
  /hello.html
```
* Dans le cas d'un paquet :
```
/application
  /__init__.py
  /templates
    /hello.html
```

Un modèle peut ressembler à ceci :
```
<!doctype html>
<title>Hello from Flask</title>
{% if name %}
  <h1>Hello {{ name }}!</h1>
{% else %}
  <h1>Hello, World!</h1>
{% endif %}
```
Dans un modèles on peut aussi utiliser les objets **config**, **request**, **session** et **g** ainsi que les fonctions **url_for()** et **get_flashed_messages()**.

## Requête

L'objet **request** permet d'accéder aux données de la requête, par exemple à son chemin avec `request.path` ou à sa méthode avec `request.methods`.

On peut également utiliser l'attribut **form** avec lequel il faut bien penser à attraper l'erreur en cas de clef inexistante sinon cela produit une erreur 404.
```
@app.route('/login', methods=['POST', 'GET'])
def login():
    error = None
    if request.method == 'POST':
        if valid_login(request.form['username'],
                       request.form['password']):
            return log_the_user_in(request.form['username'])
        else:
            error = 'Invalid username/password'
    return render_template('login.html', error=error)
```

On peut également accéder aux paramètre de l'URL (`?ket=value`) grâce à `request.args.get('key', '')`

## Fichiers

Pour que les fichiers soient transmis par le navigateur il faut définit l'attribut `enctype="multipart/form-data"` dans le formulaire HTML.

L'attribut **files** permet d'accéder aux fichiers téléchargés. Il fonctionne comme l'objet **file** de Python et possède en plus une fonction **save()** pour stocker les fichiers sur le serveur.

L'attribut **filename** n'étant pas très sécurisé (risque de falsification) il vaut mieux utiliser la fonction **secure_filename()** pour accéder au nom d'un fichier client.

```
from flask import request
from werkzeug.utils import secure_filename

@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['the_file']
        file.save(f"/var/www/uploads/{secure_filename(file.filename)}")
    ...
```

## Cookies [à renseigner]


## Gestion des erreurs

* **redirect()** permet de rediriger l'utilisateur.

* **abort()** permet d'interrompre une requête.
* Le décorateur **errorhandler()** permet de personnaliser la page d'erreur.

```
from flask import abort, redirect, url_for
from flask import render_template

@app.route('/')
def index():
    return redirect(url_for('login'))

@app.route('/login')
def login():
    abort(401)

@app.errorhandler(404)
def page_not_found(error):
    return render_template('page_not_found.html'), 404
```

Ici, **404** indique à Flask le code d'état de la page car il est à **200** (= tout est ok) par défaut.

* **make_response()** permet de récupérer l'objet de réponse pour le modifier avant de le renvoyer : `make_response(render_template('error.html'), 404)`

## API avec JSON

Lorsque l'on renvoie un `dict` depuis une vue il est automatique converti en réponse JSON.

Si l'on souhaite avoir un JSON à partir d'autre chose qu'un `dict` on peut utiliser la fonction **jsonify()**.
```
from flask import jsonify

@app.route("/users")
def users_api():
    users = get_all_users()
    return jsonify([user.to_json() for user in users])
```

## Session

**session** est un objet qui stocke des informations d'un requête à l'autre. Il signe les cookies de manière cryptographique, ces derniers sont donc visibles mais non modifiables par l'utilisateur.

Une session nécessite une clef secrète qui peut être facilement générée par `$ python3 -c 'import os; print(os.random(16))'`.
```
from flask import session

app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

@app.route('/logout')
def logout():
    session.pop('username', None)
    return redirect(url_for('index'))
```

## Messages flash

* **flash** permet de récupérer les messages à la fin d'un requête. Cependant on ne peut y accéder que lors de la prochaine requête.

* **get_flashed_messages()** peut aussi être utilisé dans ce but, notamment dans les modèles.

## Logger [à renseigner]
