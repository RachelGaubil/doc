[[_TOC_]]

# VirtualHost

Avoir un fichier `index.html`.
Créer un VirtualHost : créer /etc/sites-available/proj.conf et y mettre (pas de sécurité particulière):
```
<VirtualHost *:80>
	ServerName proj.elan-numerique.fr
	DocumentRoot /data/proj
	<Directory /data/proj/>
    Require all granted
	</Directory>
</VirtualHost>
```

Remarque : `80`-> http et `443 -> https ?`

# .htpasswd

Si on veut sécuriser, alors modifier /etc/sites-available/proj.conf :
```
<VirtualHost *:80>
	ServerName proj.elan-numerique.fr
	DocumentRoot /data/proj
	<Directory /data/proj/>
    AuthType Basic
    AuthName "Protected"
    AuthUserFile chemin/.htpasswd
    Require valid-user
	</Directory>
</VirtualHost>
```
Puis en ligne de commande éditer les utilisateurs et mots de passe :
`htpasswd chemin/.htpasswd userName`
Et ensuite entrer le mot de passe 2x.

# Dépendances

Exemple avec bootstrap : `npm install bootstrap` -> crée le package.json avec la version de bootstrap installée indiquée.
