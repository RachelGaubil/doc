| Commande | Explication |
|--------------|-----------|
| git init | transforme le dossier courant en dépôt local git |
| git remote add origin adresseSSHouHTTP | connecte le dépôt local à un nouveau projet en ligne |
| git clone adresseSSHouHTTP | récupère un dépôt git en ligne pour le cloner en local |
| git pull | récupère des éléments d'un dépôt git en ligne pour mettre à jour le dépôt local déjà existant |
| git status | affiche les différences entre les dépôts local et distant |
| git add fic | ajoute le fichier local à envoyer au dépôt distant - déclare à git de suivre ce fichier - gestion des ajouts et des modifications de fic |
| git add -u fic | idem - idem - gestion des modifications et des suppressions |
| git add -A fic | idem - idem - gestion des ajouts, des modifications et des suppressions |
| git reset | annule le *git add* |
| git commit | prépare le(s) fichier(s) du *add* à être envoyés et propose de commenter le dépôt - place le fichier (ses modifications) comme révision au sein de l'historique |
| git commit -m | idem mais le commentaire se fait à la suite de la ligne de commande |
| git push | push le(s) fichier(s) du commit vers le dépôt distant et par défaut dans la branche main |
| git log | affiche l'historique des commit |
| git stash | sert à conserver un travail qu'on ne veut pas tout de suite *add* |
| git tag vX | donne un numéro vX de version au code |
| git tag -a vX -m "version X" | idem mais ajoute une description au tag |
| git tag -l | liste l'ensemble des tags |
| git push origin vX | prise en compte du tag sur le dépôt en ligne |
| git tag -d vX | suppression du tag vX (penser à le *push* ensuite) |
| git checkout vX | récupère le projet dans un état donné au moment de vX |
| git nomBranche | crée une nouvelle branche nomBranche |
| git branch | affiche l'ensemble des branches |
| git branch -m ancienNom nouvNom | renomme une branche |
| git push origin nomBranche | push le(s) fichier(s) dans la branche nomBranche |
| git checkout nomBranche | passe de la branche courante à nomBranche |
| git checkout -b nomBranche | idem mais crée nomBranche si elle n'existe pas déjà |
| git merge nomBranche| fusionne nomBranche à la branche courante |
| git rebase nomBranche| ajoute nomBranche à la branche courante - les modifications s'ajoutent linéairement à la suite de la branche courante |
| git mergetool | affiche le fichier de la branche courante, celui avant les changements, celui que l'on veut fusionner et le résultat de la fusion |
| git branch -d nomBranche | supprime nomBranche en local |
| git push origin :nomBranche | supprime nomBranche en distant (si a bien été supprimée en local avant) |
| git fetch --prune | supprime tous les suivis locaux de branches qui n'existent plus à distance (cas ou suppression de branche à distance avant suppression en local) |
| git reflog | affiche tous les commits avec un numéro de référence (sha-1) |
| git reset --hard numX | où numX est le sha-1 : force le retour du dépôt local à l'état du commit choisi -> non pushable |
| git revert numX | où numX est le sha-1 : force le retour du dépôt local à l'état du commit choisi -> pushable |
| git config core.fileMode false | dit à git d'ignorer les changements de permissions (utile pour les pull sur le serveur) |
| git mergetool | si conflits permet d'utiliser l'outil choisit (prendre "meld") pour les résoudre en local = plus facile |
