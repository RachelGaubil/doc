[[_TOC_]]

# Généralités

Symfony est un framework (espace de travail), un ensemble d’outils avec seulement quelques règles d'emplacement de fichiers à respecter. Il est **modulable** : plus qu’un framework c’est un ensemble de librairies utilisables une à une dans n’importe quel projet (une libraire = un composant).

Symfony est un framework **MVC (ModelVueController)**, c'est-à-dire qu'il sépare les trois couches :
* Traitement - *Controller*
* Accès aux données - *Model*
* Affichages - *Vue*

Symfony s'installe avec une base de 3-4 composants à laquelle on peut ensuite ajouter des **dépendances**.
*Attention, penser à ajouter les packages nécessaires au Symfony de base (ex. : "symfony flex" pour lire les annotations)*.\
Lorsqu'on ajoute une dépendance le fichier `composer.json` est modifié automatiquement. ***(Et config ?)***

Lorsqu'on travaille avec Symfony, le **serveur** pointe automatiquement sur le fichier `public/index.php`, il faut donc le lancer sur le dossier `public`.\
Si l'on ne souhaite pas utiliser le serveur interne mais qu'on veut tout de même rester en local, il est possible de lancer le **serveur Symfony**. Ce dernier dernier a l'avantage de pouvoir activer le TLS et donc de développer directement en https pour empêcher les erreurs dues au transfert entre http et https lors de la mise en ligne de l’application Web.

L'endroit où l'on intervient le plus est le dossier `src/` car c'est là que se trouvent les *Controller*, *Entity*, etc.

# Éléments de Symfony à connaître

## Entity

Les *Entity* permettent de définir le schéma de la base de données.

La bonne pratique recommande de créer un *Controller* pour chaque *Entity*.

## Controller

Les *Controller* servent à la gestion des entrées et sorties. Ils prennent en charge une requête http et renvoient également une requête http.

Les annotations au-dessus des méthodes d'un *Controller* permettent d'indiquer les URL sans passer par le fichier `config/route.yaml`. Par ailleurs, on retrouve les paramètres de la route dans les attributs de la requête.

On y trouve la fonction *upload* qui permet d'initialiser et de soumettre le formulaire.

## Form

Les *Form* permettent de créer ou modifier des instances de classe. ***(SOIT DES ENTITY ?)***

Il y a deux types de formulaires :
* lié à une entité : map les propriétés de l'entité directement comme champs du formulaire. **(comme champs ou dans les champs ???)**
* non lié : définition manuelle des champs du formulaire.

La gestion du *Form* se fait dans un *Template*.

## Template

Les *Template* fonctionnent avec un système d'héritage de blocks. En général, on a un *Template* de base qu'on récupère et auquel on ajoute des spécificités et parfois on y inclu un autre *Template*.

Un template est un fichier `.twig` qui peut accéder aux routes des *Controller*. Il suffit de les appeler via leur *name*.

## Service

Un *Service* est un ensemble de méthodes que l'on veut mutualiser entre plusieurs *Controller*. Cela sert aussi à éviter d'encombrer ces derniers.

*EntityManager* est un *Service* ***(A VERIFIER)*** qui permet de manipuler les *Entity*. Il fait la liaison entre les *Entity* et la base de données.

## Command

*Command* permet de stocker des commandes dans `bin/console` pour pouvoir les ré-utiliser plus rapidement.

## Listener

Un *Listener* est à l'écoute d'évènements globaux. Par exemple, il peut lancer une action à la connexion.

## Repository

Un *Repository* sert à étendre la capacité des requêtes envoyées à la base de données. Il a une plus grande profondeur et gère donc des requêtes plus importantes, plus précises, plus compliquées qu'un *Controller*. Par exemple, c'est lui qui se chargera des requêtes avec jointure entre plusieurs tables.

## Autres

Le fichier `.env` contient (entre-autres) les paramètres de la base de données. C'est ici qu'est défini le docker utilisé.

Dans le dossier `config/` on peut mettre autant de fichiers qu'on le souhaite et dans `config/routes.yaml` on peut mettre autant de routes qu'on le souhaite. Symfony les prend en compte automatiquement, il n'y a donc pas besoin d'utiliser les *use*.

<!--On peut mettre le Manager en paramètre de fonction dans le contrôleur (bien penser à utiliser un use ou à le mettre en propriété du constructeur).-->

# Commandes Symfony

| Commande | Explication |
|--------------|-----------|
| symfony serve -d | lance le processus en mode démon = s’exécute en background) |
| symfony server:stop | stoppe le serveur symfony |
| symfony new project | crée un nouveau projet symfony |
| symfony new project --webapp | crée un nouveau projet symfony en y ajoutant des dépendance utiles à la création d'une application web |
| composer require nomLib | installe une dépendance (librairie) |
| php bin/console debug:autowiring | liste les services utilisables par Symfony (ceux présents sur la machine) |
| php bin/console debug:autowiring log | permet de chercher une dépendance en particulier avec un mot clef (ici "log") |
| php bin/console doctrine:schema:update --force | met à jour la base de données (création de tables) en fonction du schéma définit par les entités |
| symfony console make:controller nomController | crée classe nomController avec un code standard ainsi que le *Template* et le *Repository* associés |
