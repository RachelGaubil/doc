# OpenConnect

***Lorsque l'on souhaite utiliser un VPN, OpenConnect est une bonne alternative à Cisco sous Linux car il est beaucoup plus stable.***

## Téléchargement

Choisir la ligne de commande à copier en fonction de la distribution Linux utilisée :
* **Ubuntu** : `sudo apt install openconnect vpnc`
* **Zorin** : `sudo apt install openconnect network-manager-openconnect network-manager-openconnect-gnome -y`

## Paramétrage

Aller dans "Paramètres" puis "Réseau" et cliquer sur le "+" à côté de "VPN" :

![openconnect_1.png](img/openconnect_1.png)

Sélectionner "Multiprotocol" :

![openconnect_2.png](img/openconnect_2.png)

Dans la fenêtre qui s'affiche, remplir les champs "Nom", "VPN Protocol" et "Passerelle". On peut mettre ce qu'on veut dans "Nom" et "Passerelle" dépendera du VPN auquel on veut ce connecter (ici celui de l'UGA). Pour le "VPN Protocol il faudra toujours sélectionnner ce choix.

![openconnect_2.png](img/openconnect_3.png)

Il suffit ensuite d'activer le VPN dans les paramètres réseau.

***OpenConnect a tendance à utiliser beaucoup de batterie, c'est pourquoi il est fortement recommandé d'installer également PowerTop pour pallier à ce désagrément.***

# PowerTop

## Installation

`sudo apt-get install powertop`

## Utilisation

Lancer PowerTop avec les paramètres recommandés : `sudo powertop --auto-tune`

> Pour choisir ses propres paramètres, lancer PowerTop avec : `sudo powertop`

## Service

***L'utilisation du service permet de ne pas avoir à relancer PowerTop à chaque démarrage de la machine.***

### Création

Créer le fichier service : `sudo nano /etc/systemd/system/powertop.service`

Puis y copier les lignes suivantes :
```
[Unit]
Description=PowerTOP auto tune

[Service]
Type=oneshot
ExecStart=/usr/sbin/powertop --auto-tune
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
```

### Activation

Permettre au service de se lancer à chaque démarrage de la machine : `sudo systemctl enable powertop.service`

Activer le service : `systemctl start powertop.service`

Vérifier l'état du service : `systemctl is-active powertop.service`
